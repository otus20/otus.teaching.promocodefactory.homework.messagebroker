﻿using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Otus.Teaching.Pcf.Administration.Core.Exceptions;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Otus.Teaching.Pcf.Administration.WebHost
{
 public class Worker : BackgroundService
   {
      private ConnectionFactory _connectionFactory;
      private IConnection _connection;
      private IModel _channel;
      private readonly IServiceProvider _serviceProvider;
      private readonly IOptions<RabbitMqConfiguration> _rabbitMqOptions;

      public Worker(IServiceProvider serviceProvider, IOptions<RabbitMqConfiguration> rabbitMqOptions)
      {
         _serviceProvider = serviceProvider;
         _rabbitMqOptions = rabbitMqOptions;
      }

      public override Task StartAsync(CancellationToken cancellationToken)
      {
         _connectionFactory = new ConnectionFactory
         {
            UserName = _rabbitMqOptions.Value.UserName,
            Password = _rabbitMqOptions.Value.Password,
            HostName = _rabbitMqOptions.Value.Hostname,
            DispatchConsumersAsync = true
         };

         _connection = _connectionFactory.CreateConnection();
         _channel = _connection.CreateModel();
         _channel.QueueDeclarePassive(_rabbitMqOptions.Value.AppliedPartnerPromocodesQueue);
         _channel.BasicQos(0, 1, false);

         return base.StartAsync(cancellationToken);
      }

      protected override Task ExecuteAsync(CancellationToken stoppingToken)
      {
         stoppingToken.ThrowIfCancellationRequested();
         
         var consumer = new AsyncEventingBasicConsumer(_channel);
         
         consumer.Received += async (ch, ea) =>
         {
            if (!Guid.TryParse(Encoding.UTF8.GetString(ea.Body.ToArray()), out var managerId))
            {
               _channel.BasicNack(ea.DeliveryTag, false, false);
            }
         
            using (var scope = _serviceProvider.CreateScope())
            {
               var appliedPromoCodeManager = scope.ServiceProvider.GetRequiredService<IAppliedPromoCodeManager>();
         
               try
               {
                  await appliedPromoCodeManager.UpdateManagerPromoCodes(managerId);
               }
               catch (EmployeeNotFoundException)
               {
                  _channel.BasicNack(ea.DeliveryTag, false, false);
               }
            }
         
            _channel.BasicAck(ea.DeliveryTag, false);
         };
         
         _channel.BasicConsume(_rabbitMqOptions.Value.AppliedPartnerPromocodesQueue, false, consumer);

         return Task.CompletedTask;
      }

      public override async Task StopAsync(CancellationToken cancellationToken)
      {
         await base.StopAsync(cancellationToken);
         _connection.Close();
      }
   }
}