﻿using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost
{
   public interface IAppliedPromoCodeManager
   {
      Task UpdateManagerPromoCodes(Guid id);
   }
}