﻿using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using RabbitMQ.Client;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
   public class AdministrationGateway : IAdministrationGateway
   {
      private readonly IOptions<RabbitMqConfiguration> _rabbitMqConfiguration;
      private IConnection _connection;

      public AdministrationGateway(IOptions<RabbitMqConfiguration> rabbitMqOptions)
      {
         _rabbitMqConfiguration = rabbitMqOptions;
         CreateConnection();
      }

      private void CreateConnection()
      {
         try
         {
            var factory = new ConnectionFactory
            {
               HostName = _rabbitMqConfiguration.Value.Hostname,
               UserName = _rabbitMqConfiguration.Value.UserName,
               Password = _rabbitMqConfiguration.Value.Password
            };

            _connection = factory.CreateConnection();
         }
         catch (Exception ex)
         {
            // ignored
         }
      }

      private bool ConnectionExists()
      {
         if (_connection != null)
            return true;

         CreateConnection();

         return _connection != null;
      }

      public Task NotifyAdminAboutPartnerManagerPromoCode(Guid partnerManagerId)
      {
         if (ConnectionExists())
         {
            using var channel = _connection.CreateModel();

            channel.QueueDeclarePassive(_rabbitMqConfiguration.Value.AppliedPartnerPromocodesQueue);
            var body = Encoding.UTF8.GetBytes(partnerManagerId.ToString());

            channel.BasicPublish(
               exchange: string.Empty,
               routingKey: _rabbitMqConfiguration.Value.AppliedPartnerPromocodesQueue,
               basicProperties: null,
               body: body);
         }

         return Task.CompletedTask;
      }
   }
}