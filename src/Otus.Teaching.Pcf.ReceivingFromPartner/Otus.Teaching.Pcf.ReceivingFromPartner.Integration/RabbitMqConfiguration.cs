﻿namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
   public class RabbitMqConfiguration
   {
      public string Hostname { get; set; }
      public string UserName { get; set; }
      public string Password { get; set; }
      
      public string PromocodesQueue { get; set; }
      public string AppliedPartnerPromocodesQueue { get; set;  }
   }
}