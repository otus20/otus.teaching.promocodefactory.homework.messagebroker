﻿using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Exceptions;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost
{
   public class PromocodesManager : IPromocodesManager
   {
      private readonly IRepository<PromoCode> _promoCodesRepository;
      private readonly IRepository<Preference> _preferencesRepository;
      private readonly IRepository<Customer> _customersRepository;

      public PromocodesManager(IRepository<PromoCode> promoCodesRepository,
         IRepository<Preference> preferencesRepository,
         IRepository<Customer> customersRepository)
      {
         _promoCodesRepository = promoCodesRepository;
         _preferencesRepository = preferencesRepository;
         _customersRepository = customersRepository;
      }

      public async Task ApplyPromocodesAsync(GivePromoCodeRequest request)
      {
         // Получаем предпочтение по имени.
         var preference = await _preferencesRepository.GetByIdAsync(request.PreferenceId);

         if (preference == null)
         {
            throw new PreferenceNotFoundException();
         }

         //  Получаем клиентов с этим предпочтением.
         var customers = await _customersRepository
            .GetWhere(d => d.Preferences.Any(x => x.Preference.Id == preference.Id));

         var promoCode = PromoCodeMapper.MapFromModel(request, preference, customers);

         await _promoCodesRepository.AddAsync(promoCode);
      }
   }
}