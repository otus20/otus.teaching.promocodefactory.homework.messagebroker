﻿using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Exceptions;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost
{
   public class Worker : BackgroundService
   {
      private ConnectionFactory _connectionFactory;
      private IConnection _connection;
      private IModel _channel;
      private readonly IServiceProvider _serviceProvider;
      private readonly IOptions<RabbitMqConfiguration> _rabbitMqOptions;

      public Worker(IServiceProvider serviceProvider, IOptions<RabbitMqConfiguration> rabbitMqOptions)
      {
         _serviceProvider = serviceProvider;
         _rabbitMqOptions = rabbitMqOptions;
      }

      public override Task StartAsync(CancellationToken cancellationToken)
      {
         _connectionFactory = new ConnectionFactory
         {
            UserName = _rabbitMqOptions.Value.UserName,
            Password = _rabbitMqOptions.Value.Password,
            HostName = _rabbitMqOptions.Value.Hostname,
            DispatchConsumersAsync = true
         };

         _connection = _connectionFactory.CreateConnection();
         _channel = _connection.CreateModel();
         _channel.QueueDeclarePassive(_rabbitMqOptions.Value.PromocodesQueue);
         _channel.BasicQos(0, 1, false);

         return base.StartAsync(cancellationToken);
      }

      protected override Task ExecuteAsync(CancellationToken stoppingToken)
      {
         stoppingToken.ThrowIfCancellationRequested();

         var consumer = new AsyncEventingBasicConsumer(_channel);

         consumer.Received += async (ch, ea) =>
         {
            var content = Encoding.UTF8.GetString(ea.Body.ToArray());
            var givePromoCodeModel = JsonConvert.DeserializeObject<GivePromoCodeRequest>(content);

            using (var scope = _serviceProvider.CreateScope())
            {
               var promocodesManager = scope.ServiceProvider.GetRequiredService<IPromocodesManager>();

               try
               {
                  await promocodesManager.ApplyPromocodesAsync(givePromoCodeModel);
               }
               catch (PreferenceNotFoundException)
               {
                  // ignored
               }
            }

            _channel.BasicAck(ea.DeliveryTag, false);
         };

         _channel.BasicConsume(_rabbitMqOptions.Value.PromocodesQueue, false, consumer);

         return Task.CompletedTask;
      }

      public override async Task StopAsync(CancellationToken cancellationToken)
      {
         await base.StopAsync(cancellationToken);
         _connection.Close();
      }
   }
}