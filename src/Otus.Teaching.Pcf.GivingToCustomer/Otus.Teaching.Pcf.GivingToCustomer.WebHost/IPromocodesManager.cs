﻿using System.Threading.Tasks;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost
{
   public interface IPromocodesManager
   {
      Task ApplyPromocodesAsync(GivePromoCodeRequest request);
   }
}